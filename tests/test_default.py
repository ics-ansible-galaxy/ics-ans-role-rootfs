import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_centos7_rootfs_fstab(File):
    fstab = File('/export/nfsroots/centos7/rootfs/etc/fstab')
    assert fstab.is_file
    assert '/var' not in fstab.content
    assert ':/export/epics  /opt/epics  nfs4    defaults,async,timeo=14,ro   0   0' in fstab.content
    assert ':/export/startup  /opt/startup  nfs4    defaults,async,timeo=14,ro   0   0' in fstab.content
    assert ':/export/home  /home  nfs4    defaults,async,timeo=14,rw   0   0' in fstab.content


def test_ifc1210_rootfs_fstab(File):
    fstab = File('/export/nfsroots/ifc1210/rootfs/etc/fstab')
    assert fstab.is_file
    assert ':/export/epics  /opt/epics  nfs4    defaults,async 0  0' in fstab.content
    assert ':/export/startup  /opt/startup  nfs4    defaults,async 0  0' in fstab.content


def test_centos7_kernel(File):
    assert File('/export/images/mtcacpu-default/vmlinuz-3.10.0-693.5.2.el7.x86_64').exists
    assert File('/export/images/mtcacpu-default/vmlinuz-3.10.0-693.5.2.rt56.626.el7.centos.x86_64').exists


def test_ifc1210_kernel(File):
    kernel = 'uImage--3.14+git0+09424cee64_3428de7103-r0-ifc1210-20150917083734.bin'
    kernel_path = os.path.join('/export/images/ifc1210-default', kernel)
    assert File(kernel_path).exists
    assert File('/export/images/ifc1210-default/uImage.bin').linked_to == kernel_path
